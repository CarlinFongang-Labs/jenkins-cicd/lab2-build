#!/bin/bash
rm -rf alpinehelloworld
docker docker stop ${IMAGE_NAME} && docker rm -f ${IMAGE_NAME}
docker rmi ${IMAGE_NAME}:${IMAGE_TAG}
git clone https://github.com/CarlinFongang/alpinehelloworld.git
cd alpinehelloworld
docker build -t ${IMAGE_NAME}:${IMAGE_TAG} .