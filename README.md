# Jinkins CICD | Build

[Please find the specifications by clicking](https://github.com/eazytraining/)

------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com


><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang)


_____

## Overview of lab
1. Create a job named "build".
2. This job will build the img/image from the repository: `https://github.com/heroku/alpinehelloworld`.
3. This job will consist of a simple bash script where we will:
   - Clone the application repository to be built.
   - Build the img/image using the Dockerfile contained in the cloned repository.
4. We will use the concept of variables for the img/image name and its tag.
5. Ensure that it will be possible to override the img/image name and tag through the defined variables.

## Prerequisites
1. Install git 
2. Install Docker [Installation script here](https://gitlab.com/CarlinFongang-Labs/docker-labs/docker-install)
3. Install Docker-compose 
4. Install jenkins : [Install Jenkins on EC2](https://gitlab.com/CarlinFongang-Labs/jenkins-cicd/lab1-install)


## 1. creation du job dans jenkins 
Dashbord > New item > 
>![alt text](img/image.png)

## 2. configuration des paramettres (variables)
Dashbord > Build > Configuration 
go on "General" section > This build has settings > String Parameter
>![alt text](img/image-1.png)

1. Setting img/image_NAME
>![alt text](img/image-2.png)

2. Setting img/image_TAG
>![alt text](img/image-3.png)

## 3. Setting build script
Dashbord > Build > Configuration > Build steps
>![alt text](img/image-4.png)

Script [here](https://gitlab.com/CarlinFongang-Labs/jenkins-cicd/lab2-build/-/blob/main/script_build_jenkins.sh)

>![alt text](img/image-5.png)

## 4. Lancement du build
Dashbord > Build > Run a build with parameters
Run a "Build"
>![alt text](img/image-6.png)
*Job en cours d'exécution*
>![alt text](img/image-7.png)
*build succefull*
>![Alt text](img/image-9.png)

## 5. Sortie console
>![alt text](img/image-8.png)

















